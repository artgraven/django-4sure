from django.apps import AppConfig


class AppointmentManagerConfig(AppConfig):
    """Configuration app attributes for Appointments"""

    def ready(self):
        from appointment_manager import signal_handlers
        name = 'appointment_manager'
