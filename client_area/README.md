Client Area
-----------

this section is essentially the normal web interface and is what handles the work the client or public sees as well as providing a visual interface for clowns and troupe leaders later on.

at the moment we find a very minimalistic app.
1.  it allows logged in users to access the api
2.  it has a dashboard page
3.  there is a custom twig filter to allow us to show pages based on roles
4.  the client dashboard allows them to see all appointments before today
5.  the client dashboard: allows them to rate these by clicking smile -1 or sad -0 to rate appointment
6.  the client dashboards has 3 tabs, the first to rate, the second to see rating and the third to place booking
7.  the clown and troupe dashboard only currently gives them their api token. 

TODO: 
1.  we need to expand the other 2 templates so troupe leaders can get appointment requests and assign clowns to them
2.  add list for clowns to see their up coming events and to adjust relationship, raise issue and get address - currently only possible in api
3.  needs user experience design. i wanted to create a custom clown layout and iconography based on that but i didnt have enough time

