from typing import Any
import logging

from django.conf import settings
from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver
from django.utils import timezone

from .models import Appointment, ClownCalendar

LOGGER = logging.getLogger(__name__)


@receiver(m2m_changed, sender=Appointment.clowns.through)
def handle_clown_assignment(sender: Any, instance: Appointment, **kwargs):
    """Placeholder for the signal handler to refactor
    """

    # TODO: implement a form of email notification
