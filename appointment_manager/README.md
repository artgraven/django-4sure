Appointment Manager
-------------------


The appointment manager is really the heart of our application and api.
for us to use this we have a specific logic as defined in our model structure

1.  we assume 3 roles: clown, Troupe Leader and Client as well as super admin
2.  the Db drives down as Appointments first
3.  a troup leader creates an appointment and assigns clowns to this
4.  a signal handler adds an entry into the clown_calender for each appointment
5.  we have a have access table to manage which clowns have access to see which address
        the rest of the logic for this is yet to be implemented
6.  lastly we have a tickets table which will hold either
7.  the ratings given by clients on appointments
8.  the issues raised by clowns on appointments
9.  all of these have been implemented as an API and uses a token for the user to access it

TODO:
currently the roles is only being read by the template files and needs to be used to secure the api still
some more signal handlers should be added for the approval for clowns requesting access to information