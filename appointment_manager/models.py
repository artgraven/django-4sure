from typing import Any
import logging

from django.dispatch import receiver
from django.db.models.signals import m2m_changed
from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model

User = get_user_model()
LOGGER = logging.getLogger(__name__)

# TODO: refactor into its own section for reuse
STATUS_LEVELS = (
    (u'UP', u'upcoming'),
    (u'IN', u'incipient'),
    (u'DONE', u'completed'),
    (u'CANCEL', u'canceled'),
)

TICKET_TYPE = (
    (u'RATE', u'Rate'),
    (u'REPORT', u'Report'),
)

# TODO: some items like date and descrip should be abstracted
# so we can simply inject them into the class instead


class Appointment(models.Model):
    """ORM class for Appointment model.

    This will be our primary class to manage appointments
    """

    class Meta:
        """Metadata for the Appointment model."""
        ordering = ('created_at',)
        db_table = 'appointments'

    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=191, blank=True, null=True)
    description = models.TextField(
        help_text='Please provide more details about the event',
        blank=True)
    address = models.TextField(
        help_text='Please provide address for this appointment',
        blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL,
                                   default=1, on_delete=models.CASCADE,
                                   related_name='app_creator')
    client = models.ForeignKey(settings.AUTH_USER_MODEL, default=1,
                               on_delete=models.CASCADE, related_name='app_clients')
    clowns = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True,
                                    related_name='app_clowns')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    start_at = models.DateTimeField()
    end_at = models.DateTimeField()

    def __repr__(self):
        """String representation for our model instance."""
        return f'{self.title} (ID: {self.id})'


class Ticket(models.Model):
    """ORM class for Tickets and Ratings model.

    We use this to track Tickets related to appointments
    as well as ratings from clients
    """

    class Meta:
        """Metadata for the Tickets model."""
        ordering = ('created_by',)
        db_table = 'ticket'

    title = models.CharField(max_length=191, blank=True, null=True)
    description = models.TextField(
        help_text='Please provide more details about the event',
        blank=True)
    appointment = models.ManyToManyField(Appointment,
                                         related_name='appointments',
                                         blank=True)
    type = models.CharField(max_length=10, choices=TICKET_TYPE)
    rating_level = models.PositiveIntegerField(null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, default=1,
                                   on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        """String representation for our model instance."""
        return f'{self.title} (ID: {self.id})'


class HasAccess(models.Model):
    """ORM class for has access model.

    We use this to see which clowns have been granted detail access
    to which appointments
    """

    class Meta:
        """Metadata for the Address model."""
        ordering = ('created_at',)
        db_table = 'has_access'

    appointment = models.ManyToManyField(Appointment, related_name='acc_app',
                                         blank=True)
    requested_by = models.ForeignKey(settings.AUTH_USER_MODEL,
                                     default=1, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        """String representation for our model instance."""
        return f'{self.appointment.title} (ID: {self.id})'


class ClownCalendar(models.Model):
    """ORM class for clown calendar model.

    We use this against a signal handler for clowns to see their appointments
    and to update their status associated to them
    """

    class Meta:
        """Metadata for the Address model."""
        ordering = ('created_at',)
        db_table = 'clown_calendar'

    appointment = models.ManyToManyField(Appointment,
                                         related_name='calendar_appointment',
                                         blank=True)
    clown = models.ForeignKey(settings.AUTH_USER_MODEL,
                              default=1, on_delete=models.CASCADE)
    status = models.CharField(max_length=10, choices=STATUS_LEVELS)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        """String representation for our model instance."""
        return f'{self.appointment.title} (ID: {self.id})'

# TODO: move this to a signal handler


@receiver(m2m_changed, sender=Appointment.clowns.through)
def clowns_to_appointment(sender: Any, instance: Appointment, **kwargs):
    """Once we create an event we should add it to the calender
       of the clown attributed to them.

    1.  Add to clown calendar
    2.  Send a notification email to clown

    :param sender: the sender class that dispatched the signal
    :param instance: the appointment instance that was saved
    :param kwargs: any kwargs passed from dispatcher
    :return: None
    """
    action = kwargs.get('action')
    clown_pks = kwargs.get('pk_set')
    LOGGER.error(f'instance: {instance.id}')
    if action == 'post_add':
        try:
            for clown in clown_pks:
                user = User.objects.get(pk=clown)
                clown_calendar = ClownCalendar(clown=user,
                                               status='UP')
                clown_calendar.save()
                clown_calendar.appointment.add(instance)
        except Exception as err:
            LOGGER.error(f'Updating Clown Calendar failed (m2m): {err}')


m2m_changed.connect(clowns_to_appointment, sender=Appointment.clowns.through)
