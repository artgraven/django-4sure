from rest_framework import serializers
from .models import Appointment, Ticket, HasAccess, ClownCalendar


class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = '__all__'


class TicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket
        fields = '__all__'
        depth = 0


class HasAccessSerializer(serializers.ModelSerializer):
    class Meta:
        model = HasAccess
        fields = '__all__'


class ClownCalendarSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClownCalendar
        fields = '__all__'
