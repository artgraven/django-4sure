from django.shortcuts import render
from django.views import View
from .models import Appointment, Ticket, HasAccess, ClownCalendar
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from .serializer import AppointmentSerializer, TicketSerializer, HasAccessSerializer, ClownCalendarSerializer


# TODO: well still all of this, specifically restricting who
# can do what based on what group they are assigned to
# as we will have a group for Troupe leader, Clowns and Clients,
# we will also need to override register signal to attach a client role to that
class AppointmentViewSet(viewsets.ModelViewSet):
    serializer_class = AppointmentSerializer
    queryset = Appointment.objects.all()
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    # TODO: well we need to create a regular view for logged in clients to update their rating
    # limit the api to only return appointments for the client based on their ID and not all
    # also a view for troup leaders to create appointments and assign clowns to that


class TicketViewSet(viewsets.ModelViewSet):
    serializer_class = TicketSerializer
    queryset = Ticket.objects.all()
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    # TODO: we need to views here for creating a ticket which is either of a type
    # rating or report. here clients can rate which we must check to only be
    # earlier than current date and also where clowns can report issues
    def post(self, request):
        """ We use this so a client can rate an appointment.

            (this is mostly handled by default values so not really needed)
            :param request: we check the appointment and the rating
            :return: returns a json object
        """
        return Response('user')

    # TODO: Add a function for clowns to raise an issue


class HasAccessViewSet(viewsets.ModelViewSet):
    serializer_class = HasAccessSerializer
    queryset = HasAccess.objects.all()
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    # TODO: we must override all default methods to insure no leaks also to
    # to allow clowns to request access to addresses

    # TODO: function for clowns to request access and troupe leaders to approve


class ClownCalendarViewSet(viewsets.ModelViewSet):
    serializer_class = ClownCalendarSerializer
    queryset = ClownCalendar.objects.all()
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    # TODO: the only override here is for clowns to update the status in terms of
    # their calendar but not the actual event and also perhaps limit it so a
    # clown can only pull what is theirs and perhaps who is in the same appointment
    # this should also check the has access to bring back the address if a troupe
    # leader has approved it
