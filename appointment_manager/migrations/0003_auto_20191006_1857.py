# Generated by Django 2.2.6 on 2019-10-06 21:57

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('appointment_manager', '0002_remove_hasaccess_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClownCalendar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('UP', 'upcoming'), ('IN', 'incipient'), ('DONE', 'completed'), ('CANCEL', 'canceled')], max_length=10)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('appointment', models.ManyToManyField(blank=True, related_name='calendar_appointment', to='appointment_manager.Appointment')),
                ('clown', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'clown_calendar',
                'ordering': ('created_at',),
            },
        ),
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=191, null=True)),
                ('description', models.TextField(blank=True, help_text='Please provide more details about the event')),
                ('type', models.CharField(choices=[('RATE', 'Rate'), ('REPORT', 'Report')], max_length=10)),
                ('rating_level', models.PositiveIntegerField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('appointment', models.ManyToManyField(blank=True, related_name='appointments', to='appointment_manager.Appointment')),
                ('created_by', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'ticket',
                'ordering': ('created_by',),
            },
        ),
        migrations.DeleteModel(
            name='Issue',
        ),
    ]
