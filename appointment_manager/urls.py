from django.contrib import admin
from django.urls import path, include
from . import views
from rest_framework import routers
from .views import AppointmentViewSet, TicketViewSet, HasAccessViewSet, \
    ClownCalendarViewSet
from rest_framework.authtoken.views import obtain_auth_token

router = routers.DefaultRouter()
router.register('appointments', AppointmentViewSet)
router.register('support', TicketViewSet)
router.register('access', HasAccessViewSet)
router.register('clown_calendar', ClownCalendarViewSet)

app_name = "api"
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth', obtain_auth_token)
]

# TODO: we must add authentication to lock the api down
# here i wwant to add something like AuthTokens or JWT depending on time
# then we must implement its obain_auth_token as an (auth/)
# we must then also update settings to secure all routes for the api with it
