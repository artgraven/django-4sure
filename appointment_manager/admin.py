from django.contrib import admin
from .models import Appointment, Ticket, HasAccess, ClownCalendar


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    fields = []
    list_display = ['title', 'description', 'start_at']
    list_filter = ['start_at', 'client', 'created_by']
    search_fields = ['title']


admin.site.register(Ticket)
admin.site.register(HasAccess)


@admin.register(ClownCalendar)
class ClownCalendarAdmin(admin.ModelAdmin):
    fields = []
    list_display = ['clown', 'status']
    list_filter = ['appointment', 'clown', 'status']
    search_fields = ['appointment', 'clown', 'status']
