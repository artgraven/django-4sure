from django.shortcuts import render
from datetime import datetime
from django.http import HttpResponse
from django.views.generic.base import TemplateView
from rest_framework.authtoken.models import Token

from appointment_manager.models import Appointment, Ticket


class DashboardPageView(TemplateView):
    '''This renders the main dashboard template

        we essentially grab a twig template and inject
        into it the information needed for the roles
        return: context
    '''
    template_name = "dashboard.html"

    # Create your views here.
    def get_context_data(self, ** kwargs):
        context = super().get_context_data(**kwargs)
        context['appointments'] = Appointment.objects.filter(
            start_at__lte=datetime.now(),
            client=self.request.user)
        context['ratings'] = Ticket.objects.filter(created_by=self.request.user)
        # this is not best practice but i didnt have time to understand how to use
        # django forms and their csfr to do this correctly so this is a hack
        context['token'] = Token.objects.get(user=self.request.user)
        return context
