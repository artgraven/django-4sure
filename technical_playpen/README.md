Overview
--------

I would like to thank you for the opportunity to explore this. regardless of the outcome i really enjoyed getting a chance to play with Django 2 and the cookie cutter actually gave some cool ideas for my own further experiments.

I did not complete the full test based on my own standards so will here briefly comment on what was done and time used.

break down
==========
1. i first only did 2 hours on sunday evening the 6th and submitted the code which was the base api without authentication tokens
2. I then had 4/5 hours on the saturday afternoon of the 12th where i focussed on giving some interface by adding the dashboard, adding token authentication, adding roles and finishing up the signal handler and other url paths.
3. on sunday the 13th i only had an hour which i decided to use to just write up some readme's and make sure the code was properly formatted more or less

Things i would have loved to have gotten to and was needed to read up on.

1.  the initial setup required a bit of reading up on cookie cutter which was cool and gave me some new toys to play with
2.  i havent done template views and class based views before together this was epic
3.  the signal handler actually took me a bit as realized when i created it i had no idea how m2m_change worked so had to read up on that which i now love
4.  i wanted to move some elements such as signal handlers, choices and so on into their own files but i wasnt clear on how this was different in django 2 so i figured not to waist time and add to do.
5.  there is a bunch i would have loved to get to with regard the ui such as the plans i noted elsewhere for creating troupe and clown sections but i figured not to waist time on this i dont have as most of what i would have done would not have been new types of code or anything that would show a skill not already demonstrated so i decided to skip that for now.
6.  I would have loved to make better fictures but i havent really used them in this version and didnt have enough time to read up on them.


closing statement
-----------------

All and all i had fun creating CLOWN-HALL as i call it. i would have loved to have more time to really dig into it a bit more but unfortunantly my current work is keeping me busy till on average 10pm alongside the classes i teach and then i have limited time on weekends with family and other duties but i still thank you for the patience and if you would like to see me tackle specific challenges please let me know.

I might make some more changes but it will be after this push so there is a clear cut time as such my total working time on this project this far has been 7/8 hours including the bit of documentation.


kind regards
Jacques vd Westhuizen 